package br.com.fsp.best.controlador;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/best")
public class ControladorDeAdministrador {
	
	@RequestMapping
	public String index() {
		return "/administrador/index";
	}
	
}
