package br.com.fsp.best;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BestApp {

	public static void main(String[] args) {
		SpringApplication.run(BestApp.class, args);
	}
}
